﻿#include <iostream>
#include <ctime>

int main() {
    const int N = 5; 
    int arr[N][N];

    
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            arr[i][j] = i + j;
        }
    }

    
    std::cout << "Array:" << std::endl;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }

    
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int dayOfMonth = buf.tm_mday;

    
    int index = dayOfMonth % N;

    
    int sum = 0;
    for (int j = 0; j < N; ++j) {
        sum += arr[index][j];
    }

    
    std::cout << "Sum of elements in row " << index << ": " << sum << std::endl;

    return 0;
}
